(*************************************************************************************************************************
** ObjCakeWavelet.m
** This .m file contains a class-definition for the ObjCakeWavelet
**
** Author: T.C.J. Dela Haije <T.C.J.Dela.Haije@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
** Version: 0.1
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Objects`ObjCakeWavelet`", {
  "Classes`",
  "LieAnalysis`Objects`ObjOrientationWavelet`"
}];


DeclareClass[ObjOrientationWavelet, ObjCakeWavelet];
DeclareDefaults[ObjCakeWavelet, Association[
    "Design"              -> "N",
    "InflectionPoint"     -> 0.8,
    "SplineOrder"         -> 3,
    "MnOrder"             -> 8,
    "DcStandardDeviation" -> 3,
    "DcFilter"            -> Null,
    "FourierDcFilter"     -> Null,
    "OverlapFactor"       -> 1,
    "FourierCake"         -> Null
]];


Begin["`Private`"];


	DeclareInvariant[ObjCakeWavelet,
		(ArrayQ[#Data,3,NumberQ] || #Data === Automatic) /. False :> (Message[ValidQ::elfail, "Data", "ObjCakeWavelet", "be a numeric 3D array"];False)&];
		
  DeclareInvariant[ObjCakeWavelet, MatchQ[#Design, "N"|"M"] /. False :> (Message[ValidQ::elfail, "Design", "be either N or M"]; False)&];
  DeclareInvariant[ObjCakeWavelet,(#InflectionPoint > 0 && #InflectionPoint <= 1) /. False :> (Message[ValidQ::elfail, "InflectionPoint","be between 0 and 1"]; False)&];
  DeclareInvariant[ObjCakeWavelet, Internal`PositiveIntegerQ[#SplineOrder] /. False :> (Message[ValidQ::elfail, "SplineOrder","be a positive integer"]; False)&];
  DeclareInvariant[ObjCakeWavelet, Internal`PositiveIntegerQ[#MnOrder] /. False :> (Message[ValidQ::elfail, "MnOrder","be a positive integer"]; False)&];
  DeclareInvariant[ObjCakeWavelet, (MatrixQ[#DcFilter] || #DcFilter === Null) /. False :> (Message[ValidQ::elfail, "DcFilter","be a matrix"]; False)&];
  DeclareInvariant[ObjCakeWavelet, (MatrixQ[#FourierDcFilter] || #FourierDcFilter === Null) /. False :> (Message[ValidQ::elfail, "FourierDcFilter", "be a matrix"]; False)&];
  DeclareInvariant[ObjCakeWavelet, (Internal`PositiveIntegerQ[#OverlapFactor]) /. False :> (Message[ValidQ::elfail, "OverlapFactor", "be a positive integer"]; False)&];

  ObjCakeWavelet["Format"] = Function[{instance, fmt},
    Module[{alwaysGrid, sometimesGrid, head = Head[instance]},

    alwaysGrid = {
      BoxForm`MakeSummaryItem[{"Design: ", Subscript["\[CurlyPhi]", instance[["Design"]]]}, fmt],
      BoxForm`MakeSummaryItem[{"Orientations: ", ToString[instance["Orientations"]]<>" ("<>ToString[instance["NumberOfWavelets"]]<>")"}, fmt],
      BoxForm`MakeSummaryItem[{"WaveletSize: ", instance["WaveletSize"]}, fmt],
      BoxForm`MakeSummaryItem[{"Symmetry: ", instance[["Symmetry"]]}, fmt],
      BoxForm`MakeSummaryItem[{"Directional: ", instance[["Directional"]]}, fmt],
      BoxForm`MakeSummaryItem[{"Valid: ", ValidQ[instance]}, fmt]
    };

    sometimesGrid = {
      BoxForm`MakeSummaryItem[{"Elements: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[Keys[head["Defaults"]] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] (*&& StringStartsQ[#, "$"]*) &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
      BoxForm`MakeSummaryItem[{"Properties: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[head["Properties", "Public"] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] && StringStartsQ[#, "$"] &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
      BoxForm`MakeSummaryItem[{"ByteCount: ", ToString[ByteCount[instance]/10.^6]<>" MB"}, fmt]
    };

    BoxForm`ArrangeSummaryBox[
      head,
      SymbolName[head] <> "[<>]",
      instance["Image"],
      alwaysGrid,
      sometimesGrid,
      fmt,
      "Interpretable" -> False
    ]

  ]];

  Protect[ObjCakeWavelet];

End[];


EndPackage[];