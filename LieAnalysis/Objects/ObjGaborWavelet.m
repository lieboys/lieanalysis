(*************************************************************************************************************************
** ObjGaborWavelet.m
** This .m file contains a class-definition for the ObjGaborWavelet
**
** Author: T.C.J. Dela Haije <T.C.J.Dela.Haije@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Objects`ObjGaborWavelet`", {
  "Classes`",
  "LieAnalysis`Objects`ObjOrientationWavelet`"
}];

  (* Declare Class and Defaults *)
  DeclareClass[ObjOrientationWavelet, ObjGaborWavelet];
  DeclareDefaults[ObjGaborWavelet, Association[
      "Scale"              -> Automatic,
      "K0"                 -> {0,3},
      "Epsilon"            -> 4
  ]];


  Begin["`Private`"];

    (* Declare Incvariants *)
    DeclareInvariant[ObjGaborWavelet, (Positive[#Scale] || #Scale === Automatic) /. False :> (Message[ValidQ::elfail, "Scale", "ObjGaborWavelet", "be a positive integer"]; False)&];
    DeclareInvariant[ObjGaborWavelet, (MatchQ[#K0, {_Integer, _Integer}]) /. False :> (Message[ValidQ::elfail, "K0", "ObjGaborWavelet", "be list of two positive integers"]; False)&];
    DeclareInvariant[ObjGaborWavelet, ((NumericQ[#Epsilon] && Positive[#Epsilon])) /. False :> (Message[ValidQ::elfail, "Epsilon", "ObjGaborWavelet", "be a positive number"]; False)&];

    (* Visualization of the object *)
    ObjGaborWavelet["Format"] = Function[{instance,fmt},Module[{alwaysGrid, sometimesGrid,head = Head[instance]},

      alwaysGrid = {
        BoxForm`MakeSummaryItem[{"Orientations: ", ToString[instance["Orientations"]]<>" ("<>ToString[instance["NumberOfWavelets"]]<>")"}, fmt],
        BoxForm`MakeSummaryItem[{"WaveletSize: ", instance["WaveletSize"]}, fmt],
        BoxForm`MakeSummaryItem[{"Symmetry: ", instance[["Symmetry"]]}, fmt],
        BoxForm`MakeSummaryItem[{"Directional: ", instance[["Directional"]]}, fmt],
        BoxForm`MakeSummaryItem[{"Valid: ", ValidQ[instance]}, fmt]
      };

      sometimesGrid = {
        BoxForm`MakeSummaryItem[{"Elements: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[Keys[head["Defaults"]] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] (*&& StringStartsQ[#, "$"]*) &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
        BoxForm`MakeSummaryItem[{"Properties: ", ElisionsDump`expandablePane[Style @@@ Normal @ KeySort @ Merge[{Thread[head["Properties", "Public"] -> Plain], Thread[(Select[Keys @@ instance, StringQ[#] && StringStartsQ[#, "$"] &]) -> Bold]}, Prepend[#, Italic] &]]}, fmt],
        BoxForm`MakeSummaryItem[{"ByteCount: ", ToString[ByteCount[instance]/10.^6]<>" MB"}, fmt]
      };

      BoxForm`ArrangeSummaryBox[
        head,
        SymbolName[head] <> "[<>]",
        instance["Image"],
        alwaysGrid,
        sometimesGrid,
        fmt,
        "Interpretable" -> False
      ]

    ]];


    Protect[ObjGaborWavelet];


  End[];


EndPackage[];