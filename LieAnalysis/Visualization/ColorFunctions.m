(*************************************************************************************************************************
** ColorFunctions.m
** This .m file contains common function used for the orientation scores 2D
**
** Author: F.C. Martin <f.c.martin@tue.nl>
** Version: 0.1
**************************************************************************************************************************)

(*
	Copyright 2016 Tom Dela Haije

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*)

BeginPackage["LieAnalysis`Visualization`ColorFunctions`",{
  "LieAnalysis`Visualization`Common`",
  "LieAnalysis`Common`"
}];



  ApplyGraphicsComplexColorFunction::usage = "";
  DiagonalRGBColor::usage = "";
  EllipsoidRGBColor::usage = "";
  OrientationRGBColor::usage = "";
  Raster3DGrayLevel::usage = "";
  Raster3DRGBColor::usage = "";
  ToRGBColorList::usage = "";



  Begin["`Private`"];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    ApplyGraphicsComplexColorFunction[colorFun_, g : GraphicsComplex[pts_List, data_, opts : OptionsPattern[]], rescale : (True | False)] := Module[
      {
        fopts = FilterRules[{opts}, Except[VertexColors]],
        scaledpts = If[rescale, NormRescale[pts], pts],
        normals = OptionValue[GraphicsComplex, {opts}, VertexNormals] /. (Automatic | None) -> {},
        n = Length[pts], colors
      },

      colors = With[{min = Min[{Length[normals], n}]},

        If[min == n || min == 0,
          MapThread[colorFun, Transpose[Join[scaledpts, normals, 2]]],
          Join[
            MapThread[colorFun, Transpose[Join[scaledpts[[;; min]], normals[[;; min]], 2]]],
            MapThread[colorFun, Transpose[scaledpts]]
          ]

        ]

      ];

      GraphicsComplex[pts, data, fopts, VertexColors -> colors]

    ];
    ApplyGraphicsComplexColorFunction[_, g : GraphicsComplex[{}, _, OptionsPattern[]]] := g;

    DiagonalRGBColor[k_][d11_, _, _, d22_, _, d33_] := RGBColor[Abs[{d11, d22, d33}]];

    EllipsoidRGBColor[k_][d11_, d12_, d13_, d22_, d23_, d33_] := 
    	RGBColor[Abs[LieAnalysis`Common`EigenSystem`Private`RealSymmetricThreeEigenvectorsFromComponents[{d11}, {d12}, {d13}, {d22}, {d23}, {d33},True][[k, All, 1]]]];

    OrientationRGBColor = (RGBColor[Abs[Normalize[{#1, #2, #3}]]] &);

    Raster3DGrayLevel = (GrayLevel[#[[1]], #[[1]]] &);

    Raster3DRGBColor = (RGBColor[#[[1]], #[[2]], #[[3]], Norm[#]] &); (*Ugly because Raster3D is crap*)

    ToRGBColorList[array : {{{RGBColor[{_, _, _} | {_, _, _, _}] ...} ...} ...}] := array[[All, All, All, 1]];
    ToRGBColorList[array : {{{(RGBColor[_, _, _] | RGBColor[_, _, _, _]) ...} ...} ...}] := Apply[List, array, {3}];
    ToRGBColorList[array : {{{GrayLevel[{_, _}] ...} ...} ...}] := Join[array[[All, All, All, 1, {1}]], array[[All, All, All, 1, {1}]], array[[All, All, All, 1, {1}]], array[[All, All, All, 1, {2}]], 4];
    ToRGBColorList[array : {{{GrayLevel[_, _] ...} ...} ...}] := Apply[List, array, {3}][[All, All, All, {1, 1, 1, 2}]];
    ToRGBColorList[array : {{{GrayLevel[{_}] ...} ...} ...}] := Join[array[[All, All, All, 1]], array[[All, All, All, 1]], array[[All, All, All, 1]], 4];
    ToRGBColorList[array : {{{GrayLevel[_] ...} ...} ...}] := Apply[List, array, {3}][[All, All, All, {1, 1, 1}]];
    ToRGBColorList[array_List] := ToRGBColorList[Map[ColorConvert[#, RGBColor] &, array, {3}]];



  End[];



EndPackage[];