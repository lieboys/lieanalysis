(*************************************************************************************************************************
** Rotations.m
** This .m file contains functions to rotate vectors
** 
** Author: M.H.J. Janssen <m.h.j.janssen@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Common`Rotations`"];

	Begin["`Private`"];

		
		
		RnWithAlphaIs0 = Compile[
			
			{{n,_Real,1}},
			
			Module[
				
				{
					x,y,z,beta=0.,gamma=0.,
					epsilon=10^-8
				},
				
				{x,y,z} = n / Sqrt[ n[[1]]^2 + n[[2]]^2 + n[[3]]^2 ];
            
             	If[z < 1-epsilon,
					If[z > -1 + epsilon,
						{beta,gamma}={ArcCos[z],ArcTan[x,y]};,
                        {beta,gamma}={\[Pi],0.};
                   	];,
					{beta,gamma}={0.,0.};
                ];
                
                {
					{Cos[beta] Cos[gamma],-Sin[gamma],Cos[gamma] Sin[beta]},
                    {Cos[beta] Sin[gamma],Cos[gamma],Sin[beta] Sin[gamma]},
                    {-Sin[beta],0.,Cos[beta]}
                }
			],
			{RuntimeAttributes->{Listable}}
		
		];
		
	
	End[];
	
EndPackage[];