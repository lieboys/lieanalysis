(*************************************************************************************************************************
** ExternalRegularization.m
** This .m file contains functions to use external regularization upon gauge frames
**
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Common`ExternalRegularization`"];



	Begin["`Private`"];
	
	Needs["LieAnalysis`Common`CoordinateSystemConversions`"];
	Needs["LieAnalysis`Common`SphericalHarmonics`"];
	
	
	(* External Regularization for Tensors from 2D orientation scores *)
	ExternalRegularization[tensor_ /; MatchQ[Dimensions[tensor],{_,_,_,3,3}], orientations_, {sigmaSpatialExternal_, sigmaAngularExternal_}] := Block[
	
		{
			sigmas = {sigmaSpatialExternal, sigmaSpatialExternal, sigmaAngularExternal}
		},
		
		(* change of basis to cartesian  *)
		tensor = LieAnalysis`Common`FromLeftInvariantFrame[orientations ,tensor];
		
		(* Apply external regularization *)
		tensor = Transpose[Map[
			GaussianFilter[#,{4sigmas+1,sigmas}]&,Transpose[tensor,{3,4,5,1,2}],{2}
		],{4,5,1,2,3}];
		
		(* change basis to left-invariant frame *)
		tensor = LieAnalysis`Common`ToLeftInvariantFrame[orientations ,tensor];
		
		(* Return the object*)
		Return[tensor];
	
	]

		
	(* External Regularization for Tensors from 3D Orientation Scores *)
	ExternalRegularization[tensor_ /; MatchQ[Dimensions[tensor],{_,_,_,_,6,6}], orientations_, {sigmaSpatialExternal_, sigmaAngularExternal_}] := Block[
	  		
	  	{
	  		lMax = LieAnalysis`Common`SphericalHarmonicsLMax[Length[orientations]],
	  		sigmas = {sigmaSpatialExternal,sigmaSpatialExternal,sigmaSpatialExternal},
	  		tensorData, shCoefficients, beta, gamma
	  	},
	  		
	  	(* Get Polar Coordinate List *)
	  	{beta,gamma} =  Transpose[LieAnalysis`Common`CartesianToSphericalCoordinates[orientations]][[2;;3]];
	  		
	  	(* Change to Cartesian basis *)
	  	tensorData = LieAnalysis`Common`FromLeftInvariantFrame[orientations, tensor]; (* x,y,z,R,6,6*)
	  		
	  	(* Apply Spatial Regularization *)
	  	tensorData = Developer`ToPackedArray@Map[
	  		GaussianFilter[#,{4sigmas+1,sigmas}]&,
	  		Transpose[tensorData,{4,5,6,3,1,2}],{3}
	  	]; (* 6,6,R,x,y,z*)
	  	
	  	(* Apply Angular Regularization *)
	  	shCoefficients=Table[Developer`ToPackedArray@LieAnalysis`Common`RealSphericalHarmonicFit[orientations,Transpose[tensorData[[i,j,All,All,All,All]],{4,1,2,3}],lMax] ,{i,1,6,1},{j,1,6,1}]; (* 6,6,x,y,z,R*)
		shCoefficients=Developer`ToPackedArray[shCoefficients];
	  	shCoefficients = LieAnalysis`Common`SphericalHarmonicsAngularBlur[shCoefficients,sigmaAngularExternal];
	  		
	  	(* Evaluate SH Coefficients *)
	  	tensorData = Transpose[shCoefficients.Transpose@(LieAnalysis`Common`SphericalHarmonicsTable[0, lMax][beta,gamma]),{5,6,1,2,3,4}];(* x,y,z,R,6,6*)
	  		
	  	(* Back To Left Invariant Frame *)
	  	tensorData = LieAnalysis`Common`ToLeftInvariantFrame[orientations,tensorData];
	  		
	  	(* Returns Smoothed Tensor *)
		Return[tensorData];
	  		
	];
	
	End[];
	
	
EndPackage[];