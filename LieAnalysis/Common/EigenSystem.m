(*************************************************************************************************************************
** EigenSystem.m
** This .m file contains functions to compute eigenvalues and eigenvectors for the symmetric 3x3 case
**
** Author: T.C.J. Dela Haije <t.c.j.dela.haije@tue.nl> 
** Author: E.J. Bekkers <e.j.bekkers@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Common`EigenSystem`",{
	"LieAnalysis`"
}];



	Begin["`Private`"];



		Needs["LieAnalysis`Common`Messages`"];
		

		(* Common (public) functions *)



		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* RealSymmetricThreePositiveDefiniteQ - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



		RealSymmetricThreePositiveDefiniteQ[tensorComponents_ /; ArrayQ[tensorComponents, 5] && Last[Dimensions[tensorComponents]] === 6] := 
			RealSymmetricThreePositiveDefiniteMask @@ Transpose[tensorComponents, {2, 3, 4, 5, 1}];		
		
		
		
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* RealSymmetricThreeEigensystem - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		
		
		
		Options[RealSymmetricThreeEigensystem] = {
			"SortOnAbsoluteValue"->True,
			"Output"->"Eigenvectors"
		};
		
		
		RealSymmetricThreeEigensystem[tensorComponents_, opts:OptionsPattern[]] := Module[
			
			{
				sortQ = OptionValue["SortOnAbsoluteValue"],
				f, tmp
			},
			
			BooleanQ[sortQ] /. False :> (
				Message[General::argfail,"SortOnAbsoluteValue", "a boolean value"];Abort[];
			);
			
			f = Switch[ToUpperCase@OptionValue["Output"],
				"EIGENVECTORS", RealSymmetricThreeEigenvectorsFromOrientationScores3D,
				"EIGENVALUES", RealSymmetricThreeEigenvaluesFromOrientationScores3D,
				"EIGENSYSTEM", RealSymmetricThreeEigensystemFromOrientationScores3D,
				_, Message[General::argfail, "Output", "be Eigenvalues, Eigenvectors or Eigensystem"]
			];
			
			(* x,y,z,theta,{a,b,c,d,e,f} *)
			(ArrayQ[tensorComponents,5] && Last@Dimensions[tensorComponents] === 6) /. True :> (
				Return@f[tensorComponents,sortQ]
			);
			
			(* x,y,z,theta,{3x3-matrix} *)
			(ArrayQ[tensorComponents,6] && Dimensions[tensorComponents][[-2;;-1]] === {3,3}) /. True :> (
				tmp = Transpose[Flatten[Transpose[tensorComponents,{3,4,5,6,1,2}],1][[{1,2,3,5,6,9}]],{5,1,2,3,4}];
				Return@f[tmp,sortQ]
			);
			
			(* x,y,theta, {a,b,c,d,e,f} *)
			(ArrayQ[tensorComponents, 4] && Last@Dimensions[tensorComponents] === 6) /. True :> (
				Return@First@f[{tensorComponents},sortQ]
			);
			
			(* x,y,theta,{3x3-matrix} *)
			(ArrayQ[tensorComponents, 5] && Dimensions[tensorComponents][[-2;;-1]] === {3,3}) /. True :> (
				tmp = Transpose[Flatten[Transpose[tensorComponents,{3,4,5,1,2}],1][[{1,2,3,5,6,9}]],{4,1,2,3}];
				Return@First@f[{tmp},sortQ]
			);
			
			Message[General::argfail,"RealSymmetricThreeEigenvalues", "Input dimensions not recognised"];
			
		]
		
	
		
		(* Some helper functions  *)
		
		
		
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* RealSymmetricThreePositiveDefiniteMask- - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		
		
		
		RealSymmetricThreePositiveDefiniteMask = Compile[
			
			{
				{a, _Real, 4}, {b, _Real, 4}, {c, _Real, 4}, {d, _Real, 4}, {e, _Real, 4}, {f, _Real, 4}
			},
	
			Module[
				
				{
					c0 = a e^2 + d c^2 + f b^2 - a d f - 2 b c e, 
					c1 = a d + a f + d f - (b^2 + c^2 + e^2), 
					c2 = -(a + d + f), 
					p, q, phi,\[Epsilon]=10.^(-6)
				},
		   
		   		p = c2^2 - 3 c1;
			       q = -27/2 c0 - c2 (9/2 c1 - c2^2);
		   
		   		phi = 1/3 Arg[q + I Sqrt[Chop[27 (1/4 c1^2 (p - c1) + c0 (q + 27/4 c0)),\[Epsilon]]]];
		   
		   		1 - UnitStep[- Sqrt[p] 2 Cos[phi + (2 Pi)/3] - c2]
		   
			]
		  
		];
		

		
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* RealSymmetricThreeEigenvaluesFromComponents - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		
		
		
		SortAbs3 = Compile[
			
			{
				{a,_Real},{b,_Real},{c,_Real}
			},
			
			Module[
				
				{
					aAbs = Abs[a],
					bAbs = Abs[b],
					cAbs = Abs[c]
				},
				
				(* Sort a,b,c *)
				If[aAbs>bAbs,
					
					(* A > B*)
					If[bAbs>cAbs,
						Return[{a,b,c}], (* A > B & B > C*)
						If[aAbs>cAbs,
							Return[{a,c,b}], (* A > B & B < C & A > C *)
							Return[{c,a,b}]  (* A > B & B < C & C > A *)
						]
					],
						
					(* A < B *)
					If[bAbs<cAbs,
						Return[{c,b,a}], (* A < B & B < C *)
						If[aAbs>cAbs,
							Return[{b,a,c}], (* A < B & C < B & A > C *)
							Return[{b,c,a}]  (* A < B & C < B & A < C *)
						]
					]
				]
			],
				
			RuntimeAttributes-> {Listable}
				
			];
		
		
		(* Eigenvalue function from here on *)
		
		
		
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* RealSymmetricThreeEigenvaluesFromComponents - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		
		
		
		RealSymmetricThreeEigenvaluesFromComponents = Compile[

			(* Input Pattern *)			
			{
				{a, _Real, 1}, {b, _Real, 1}, {c, _Real, 1}, {d, _Real, 1}, {e, _Real, 1}, {f, _Real, 1},{sortOnAbsoluteValue,True|False}
			},
   
			Module[
				
				(* Local Variable Declaration *)
				{
					c0,c1,c2, 
					epsilon=10.^(-6),
					p, q, phi,val1,val2,val3,
		   			an, bn, cn, dn, en, fn, norms
		   		},
		   		(* Normalize tensor components (for stability of the algorithm)*)
				norms = Sqrt[a^2+b^2+c^2+d^2+e^2+f^2] /. 0->1; 
				{an,bn,cn,dn,en,fn} = Map[#/norms&,{a,b,c,d,e,f}];
		    
		    	c0 = an en^2 + dn cn^2 + fn bn^2 - an dn fn - 2 bn cn en; 
				c1 = an dn + an fn + dn fn - (bn^2 + cn^2 + en^2); 
				c2 = -(an + dn + fn);
					
				p = c2^2 - 3 c1;
			    q = -27/2 c0 - c2^3+ 9/2 c1 c2;
			    
				phi = 1/3 Arg[q + I Sqrt[Chop[27 (1/4 c1^2 (p - c1) + c0 (q + 27/4 c0)),epsilon]]];	    
		
			    val1 = Sqrt[p]/3 2 Cos[phi] - c2/3;
				val2 = Sqrt[p]/3 (-Cos[phi] + Sqrt[3] Sin[phi]) - c2/3;
				val3 = Sqrt[p]/3 (-Cos[phi] - Sqrt[3] Sin[phi]) - c2/3;
				
				(* Scale eigenvalues*)
				{val1,val2,val3} = Map[#*norms&,{val1,val2,val3}]; 
							
				(* Return (sorted) eigen values *)
				If[sortOnAbsoluteValue,
					Transpose[SortAbs3[val1,val2,val3]],
					{val1,val2,val3}
				]    
				
			],
			
			CompilationOptions->{"InlineExternalDefinitions"-> True}
			
		];
		
		
		
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* RealSymmetricThreeEigenvaluesFromOrientationScores- - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		
		
		(* TODO: should be accesable *)
		RealSymmetricThreeEigenvaluesFromOrientationScores3D = Compile[
			
			(* Input Pattern *)
			{
				{tensorComponents,_Real,5},{sortOnAbsoluteValue,True|False}
			},
			
			Module[
				
				(* Local Variable Declaration *)
				{
					dim = Dimensions[tensorComponents, 4],
					flatcomp = Flatten[tensorComponents, 3],
					eval
				},
			
				(* Compute Eigenvalues *)
				eval = RealSymmetricThreeEigenvaluesFromComponents[flatcomp[[All,1]],flatcomp[[All,2]],flatcomp[[All,3]],flatcomp[[All,4]],flatcomp[[All,5]],flatcomp[[All,6]],sortOnAbsoluteValue];
				
				(* Reshape to original dimentions *)
				Partition[Partition[Partition[Transpose[eval],dim[[4]]],dim[[3]]],dim[[2]]]
				
			]
			,
			
			CompilationOptions->{"InlineExternalDefinitions"-> True}
			
		];
		
		
		
		
		
		
		(* Eigenvector functions from here on *)
		
		
			
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* CalculateEigenvectors - - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		
		
		
		CalculateEigenvectors = Compile[
			
			(* Argument Pattern *)
			{
				{a, _Real}, {b, _Real}, {c, _Real}, {d, _Real}, {e, _Real}, {f, _Real}, {l1, _Real}, {l2, _Real}, {l3, _Real}
			},
			
			
			Module[
				
				(* Local Variable Declaration *)
				{
					mu=0.,
					epsilon=10.^(-6),
					ev1,ev2,ev3,same12,same13,same23
				},
				
				(* Find same eigenvalues *)
				same12 = Chop[l1-l2,epsilon]==0;
				same23 = Chop[l2-l3,epsilon]==0;
				same13 = Chop[l1-l3,epsilon]==0;
				
				(* 3 Eigenvalues are the same *)
				{ev1,ev2,ev3} = {{1.,0.,0. },{0.,1.,0. },{0. ,0. ,1.}};
				If[same12&&same23&&same13, Return[{{1.,0.,0. },{0.,1.,0. },{0. ,0. ,1.}}]];
				
				(*Check for diagonal matrix*)
				If[Chop[e,epsilon]==0.&&Chop[c,epsilon]==0.&&Chop[b,epsilon]==0.,
					
					If[Chop[a-l1,epsilon]==0.,ev1={1.,0.,0. },If[Chop[d-l1,epsilon]==0.,ev1={0.,1.,0.},ev1={0.,0.,1.}]];
					If[Chop[a-l2,epsilon]==0.,ev2={1.,0.,0. },If[Chop[d-l2,epsilon]==0.,ev2={0.,1.,0.},ev2={0.,0.,1.}]];
					If[Chop[a-l3,epsilon]==0.,ev3={1.,0.,0. },If[Chop[d-l3,epsilon]==0.,ev3={0.,1.,0.},ev3={0.,0.,1.}]];
				
					(* 2 eigenvalues are the same *)
					If[same12,ev2=Cross[ev1, ev3]];   	
					If[same23,ev3=Cross[ev2, ev1]];   
					If[same13,ev3=Cross[ev1, ev2]];
				
					Return[{ev1,ev2,ev3}]
				];
				
				(*Outer product of first and second column of A-\[Lambda] I*) 
				ev1 = {b e - c d + c l1,c b - e a + e l1,(a - l1) (d - l1) - b^2};
				ev2 = {b e - c d + c l2,c b - e a + e l2,(a - l2) (d - l2) - b^2};
				ev3 = {b e - c d + c l3,c b - e a + e l3,(a - l3) (d - l3) - b^2};
				
				(*Use alternative formula when first and second column are linearly dependent*)
				If[Chop[ev1,epsilon]=={0.,0.,0.},
					If[Chop[e,epsilon]==0.,
						If[Chop[c,epsilon]==0.,mu=(a-l1)/b;ev1=1/Sqrt[1+Abs[mu]^2] {1.,-mu,0.};,ev1={0.,1.,0.};];,
						mu=c/e;ev1=1/Sqrt[1+Abs[mu]^2] {1,-mu,0};
					];
				];
				
				If[Chop[ev2,epsilon]=={0.,0.,0.},
					If[Chop[e,epsilon]==0.,
						If[Chop[c,epsilon]==0.,mu=(a-l2)/b;ev2=1/Sqrt[1+Abs[mu]^2] {1.,-mu,0.};,ev2={0.,1.,0.};];,
						mu=c/e;ev2=1/Sqrt[1+Abs[mu]^2] {1,-mu,0};
					];
				];
				
				If[Chop[ev3,epsilon]=={0.,0.,0.},
					If[Chop[e,epsilon]==0.,
						If[Chop[c,epsilon]==0.,mu=(a-l3)/b;ev3=1/Sqrt[1+Abs[mu]^2] {1.,-mu,0.};,ev3={0.,1.,0.};];,
						mu=c/e;ev3=1/Sqrt[1+Abs[mu]^2] {1,-mu,0};
					];
				];
				
				(*In case 2 eigenvalues are the same only one eigenvector is found, second eigenvector is found using the outer product of the other two eigenvectors*)
				If[same12, ev2 = Cross[ev1, ev3]];   	
				If[same23, ev3 = Cross[ev2, ev1]];   
				If[same13, ev3 = Cross[ev1, ev2]];    
				
				(* Return the Eigen Vectors *)
				Return[{ev1,ev2,ev3}]
				
			]
			
			,RuntimeAttributes->{Listable}
		];
		
		
		
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* NormalizeEigenvectors - - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		
		
		
		NormalizeEigenvectors = Compile[
			
			(* Argument Pattern *)
			{
				{v1,_Real,2},{v2,_Real,2},{v3,_Real,2}
			},
			
		   	Module[
		   		
		   		(* Local Variable Declaration *)
		   		{ 
		   			n1, n2, n3
	   			},
			    
			    (* Compute length of the vectors *)
			    {n1, n2, n3} = Chop[{
			    	Sqrt[v1[[1]]^2 + v1[[2]]^2 + v1[[3]]^2],
			    	Sqrt[v2[[1]]^2 + v2[[2]]^2 + v2[[3]]^2],
			       	Sqrt[v3[[1]]^2 + v3[[2]]^2 + v3[[3]]^2]
				}];
		
				(* Savely divide by the length to normalize *)
				Return[{
					Unitize[n1] Transpose[v1]/(n1 + 1 - Unitize[n1]),
					Unitize[n2] Transpose[v2]/(n2 + 1 - Unitize[n2]),
					Unitize[n3] Transpose[v3]/(n3 + 1 - Unitize[n3])
				}]
				
			]  
		];
		
		
		
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* RealSymmetricThreeEigenvectorsFromComponents- - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		
		
		
		RealSymmetricThreeEigenvectorsFromComponents = Compile[
			
			(* Input Pattern *)
			{
				{a, _Real, 1}, {b, _Real, 1}, {c, _Real, 1}, {d, _Real, 1}, {e, _Real, 1}, {f, _Real, 1},{sortOnAbsoluteValue,True|False}
			},
   
		   	Module[
		   		
		   		(* Local Variable Declaration *)
		   		{
		   			l1, l2, l3, v1, v2, v3,
		   			an, bn, cn, dn, en, fn, norms
		   		},
		   		(* Normalize tensor components (for stability of the algorithm)*)
				norms = Sqrt[a^2+b^2+c^2+d^2+e^2+f^2] /. 0->1; 
				{an,bn,cn,dn,en,fn} = Map[#/norms&,{a,b,c,d,e,f}];
				
		   		(* Compute Eigen Values *)
		        {l1, l2, l3} = RealSymmetricThreeEigenvaluesFromComponents[an, bn, cn, dn, en, fn,sortOnAbsoluteValue];
		        
		        (* Compute Eigen Vectors and normalize them *)
			    {v1, v2, v3} = Transpose[CalculateEigenvectors[an, bn, cn, dn, en, fn,l1,l2,l3],{3,1,2}];
			    {v1, v2, v3} = Transpose[NormalizeEigenvectors[v1,v2,v3],{1,3,2}];
		
				(* Return the Eigenvectors *)
				{v1,v2,v3} 	    
				
			]
			,
			
			CompilationOptions->{"InlineExternalDefinitions"-> True}  
		];
		
		
		
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* RealSymmetricThreeEigenvectorsFromOrientationScore- - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		
		
		(* TODO: should be accesable *)
		RealSymmetricThreeEigenvectorsFromOrientationScores3D = Compile[
			
			(* Input Pattern *)
			{
				{tensorComponents,_Real,5},{sortOnAbsoluteValue,True|False}
			},
   
		   	Module[
		   		
		   		(* Local Variable Declaration *)
		   		{
		   			dim = Dimensions[tensorComponents, 4],
			    	flatcomp = Flatten[tensorComponents, 3],
			    	v
			    },
		    
		    	(* Compute Eigenvectors *)
			    v = RealSymmetricThreeEigenvectorsFromComponents[flatcomp[[All,1]],flatcomp[[All,2]],flatcomp[[All,3]],flatcomp[[All,4]],flatcomp[[All,5]],flatcomp[[All,6]],sortOnAbsoluteValue];	    
			    
			    (* Reshape to original dimensions *)
				Partition[Partition[Partition[Transpose[v,{2,3,1}],dim[[4]]],dim[[3]]],dim[[2]]]
		
			] 
			,
			
			CompilationOptions->{"InlineExternalDefinitions"-> True} 
		];
		


		(* Eigenvector functions from here on *)
		
		
		
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* RealSymmetricThreeEigensystemFromComponents - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		
		
		
		RealSymmetricThreeEigensystemFromComponents = Compile[
			
			{
				{a, _Real, 1}, {b, _Real, 1}, {c, _Real, 1}, {d, _Real, 1}, {e, _Real, 1}, {f, _Real, 1},{sortOnAbsoluteValue,True|False}
			},
   
		   	Module[
		   		
		   		(* Local Variable Declaration *)
		   		{
		   			l1, l2, l3, v1, v2, v3,
		   			an,bn,cn,dn,en,fn, norms
		   		},
		   		(* Normalize tensor components (for stability of the algorithm)*)
				norms = Sqrt[a^2+b^2+c^2+d^2+e^2+f^2] /. 0->1; 
				{an,bn,cn,dn,en,fn} = Map[#/norms&,{a,b,c,d,e,f}];
		   		
		   		(* Compute Eigenvalues *)
			    {l1, l2, l3} = RealSymmetricThreeEigenvaluesFromComponents[an,bn,cn,dn,en,fn,sortOnAbsoluteValue];
			    
			    (* Compute Eigenvectors and normalize them *)
			    {v1, v2, v3} = Transpose[CalculateEigenvectors[an, bn, cn, dn, en, fn,l1,l2,l3],{3,1,2}];
			    {v1,v2,v3} = Transpose[NormalizeEigenvectors[v1,v2,v3],{1,3,2}];
		
				(* Return both the Eigenvalues and Eigenvectors *)
				{{l1*norms, l2*norms, l3*norms},v1,v2,v3} 			    
			]
			,
			
			CompilationOptions->{"InlineExternalDefinitions"-> True}
		   
		];
		
		
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* RealSymmetricThreeEigensystemFromComponents - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		
		
		
		(* TODO: should be accesable *)
		RealSymmetricThreeEigensystemFromOrientationScores3D = Compile[
			
			(* Input Pattern *)
			{
				{tensorComponents,_Real,5},{sortOnAbsoluteValue,True|False}
			},
		   
		   	Module[
		   		
		   		(* Local Variable Declaration *)
		   		{
				    dim = Dimensions[tensorComponents, 4],
				    flatcomp = Flatten[tensorComponents, 3],
				    lambda, v1, v2, v3
				},
		
				(*  *)
			    {lambda, v1, v2, v3} = RealSymmetricThreeEigensystemFromComponents[flatcomp[[All,1]],flatcomp[[All,2]],flatcomp[[All,3]],flatcomp[[All,4]],flatcomp[[All,5]],flatcomp[[All,6]],sortOnAbsoluteValue];
		
				Partition[Partition[Partition[Transpose[{lambda, v1, v2, v3},{2,3,1}],dim[[4]]],dim[[3]]],dim[[2]]]			    
			]
			,
			
			CompilationOptions->{"InlineExternalDefinitions"-> True}
		   
		];
		
		
		
	End[];
	


EndPackage[]