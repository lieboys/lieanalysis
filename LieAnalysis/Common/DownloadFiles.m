(*************************************************************************************************************************
** CUDALibraryLoad.m
** This .m file contains functions to load custom CUDA functions
**
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Common`DownloadFiles`"];



	Begin["`Private`"];
	
	
	
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	    (* DownloadFile- - - - - - - - - - - - - - - - - - - - - - - *)
	    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
	    
	    
		DownloadFile[url_, location_, time_:5] := Module[
	
			{
				fileName = FileBaseName[url]<>"."<>FileExtension[url],
				tmpFileName = FileNameJoin[{$TemporaryDirectory, "tmp."<>FileExtension[url]}],
				progress,output, printTmp
			},
			
			(* delete file *)
			If[FileExistsQ[tmpFileName],If[DeleteFile[tmpFileName]=== $Failed, Message[General::filefail, "DeleteFile", "Deleting file: "<>tmpFileName<>" failed"];Abort[]]];
			
			(* Progress indicator *)
			progress=0.;
			printTmp = If[OptionValue[LieAnalysis`LieAnalysis,"DynamicUpdates"], PrintTemporary[Row[{"Downloading: ",ProgressIndicator@Dynamic@progress, " ("<>url<>")"}]]];
			progFunction[_, "progress", {dlnow_, dltotal_, _, _}]:= (Quiet[progress=dlnow/dltotal;]);
			
			(* Download *)
			WaitAsynchronousTask[URLSaveAsynchronous[url, tmpFileName, progFunction, "Progress"->True],"Timeout"->time];
			
			(* Check if the download has finisched *)
			If[progress != 1, Message[General::netfail, "download", "download timedout"]; Abort[]];
			
			(* Store the file in the location *)
			output = CopyFile[tmpFileName, FileNameJoin[{location,fileName}]];
			
			(* Return the file location *)
			Return[output];
			
		];
	
	
	
	End[];
	
	
	
EndPackage[];

	