(*************************************************************************************************************************
** CUDALibraryLoad.m
** This .m file contains functions to load custom CUDA functions
**
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Common`CUDALibraryLoad`"];



	Begin["`Private`"];
	
		Get["LieAnalysis`Common`"];
		
		LoadCUDAFunction[functionName_] := Module[
			{
				baseUrl = LieAnalysis["CUDALibrariesURL"],
				libraryName = "CUDAOrientationScoreTransform",
				cufftName = "cufft64_80.dll",
				cudaLibraryLocation = FileNameJoin[Join[(FileNameSplit@FindFile["LieAnalysis`"])[[1;;-3]],{"CUDALibraries"}]],
				function, libFile, cufftFile 
			},
			
			libFile = FileNameJoin[{cudaLibraryLocation, libraryName<>".dll"}];
			
			cufftFile = FileNameJoin[{cudaLibraryLocation, cufftName}];
			
			(* Create Directory if needed *)
			If[!DirectoryQ[cudaLibraryLocation],CreateDirectory[cudaLibraryLocation]];
			
			(* Download File if needed *)
			If[!FileExistsQ[libFile],
				DownloadFile[Echo[URLBuild[{baseUrl, libraryName<>".dll"}],"Downloading: "], cudaLibraryLocation, 10];
			];
			
			(* Download Cufft if needed *)
			If[!FileExistsQ[cufftFile],
				DownloadFile[Echo[URLBuild[{baseUrl, cufftName}],"Downloading (100MB): "], cudaLibraryLocation,600];
			];
			
			(* Load the function *)
			function = Switch[functionName,
				
				"orientationScoreTransform", 
					LibraryFunctionLoad[libFile, "orientationScoreTransform",{{Real, 1}, Integer, Integer, {Real, 1}, Integer, Integer}, {Real, 1}]
			
			];
			
			(* Return function object *)
			Return[function]
			
			
		]
	
	
	End[];
	
	
	
EndPackage[];
