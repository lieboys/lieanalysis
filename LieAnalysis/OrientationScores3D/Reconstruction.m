(*************************************************************************************************************************
** Reconstruction.m
** This .m file contains functions used for the reconstrucion of 3D Orientation Scores.
**
** Author: M.H.J.Janssen <m.h.j.janssen@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
** Version: 0.1
**************************************************************************************************************************)
BeginPackage["LieAnalysis`OrientationScores3D`Reconstruction`",{
  "LieAnalysis`"
}];



  Begin["`Private`"];

    Needs["Classes`"];
    Get["LieAnalysis`Common`"];
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* InverseOrientationScoreTransform3D- - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    Options[InverseOrientationScoreTransform3D] = {
      "Method" -> "Summation",
      "UseDcComponent" -> True
    };



    InverseOrientationScoreTransform3D::optfail = "The value provided for option `1` is not recognised. Should `2`";



	InverseOrientationScoreTransform3D[ost3dObjIn_/;ValidQ[ost3dObjIn, LieAnalysis`Objects`Obj3DPositionOrientationData`Obj3DPositionOrientationData], opts:OptionsPattern[]] := Module[

	  	(* Local Variables Declaration *)
		{
		    image,
		    method = OptionValue["Method"],
			useDC = OptionValue["UseDcComponent"],
		    ost3dObj = ost3dObjIn
	  	},

		(* Validate Input *)
	    And[
			MatchQ[method, "Summation" | "L2" |"Exact" | "RknAdjoint" | "L2adjoint" ]
	        	/. False :> (Message[InverseOrientationScoreTransform3D::optfail, "Method","be either \"Summation\", \"L2adjoint\" or \"RknAdjoint\""]),
	
	        BooleanQ[useDC]
	            /. False :> (Message[InverseOrientationScoreTransform3D::optfail, "UseDcComponent", "a boolean value"]; useDC = True)
	 	];

      	(* Remove DC-component *)
		If[!useDC,
      		ost3dObj = Affix[ost3dObj, "DcData"->ConstantArray[0, Dimensions[ost3dObj["FullData"][[All,All,All,1]]]]]
      	];

      	(* Execute the chosen reconstruction method *)
      	image = Switch[method,
	        "Exact" | "RknAdjoint", Exact[ost3dObj],
	        "L2" | "L2adjoint", L2Adjoint[ost3dObj],
	        "Summation" | _, Summation[ost3dObj]
      	];

      	(* Return the Orientation Score Transformed image *)
      	Return[Image3D[image,"Real32"]];

    ];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* Summation - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    Summation[ost3dObj_] := Module[

		{
        	reconstruction
      	},

		(* Integrate Orientations and scale by integration step-size *)
      	reconstruction = Plus@@Reorder[ost3dObj["FullData","Real"],"OrientationFirst"] ( 4*Pi / ost3dObj["Orientations"]);
      
      	(* Add the DC-component *)
      	reconstruction = reconstruction + ost3dObj[["DcData"]];

      	(* Return the re-constructed volume *)
      	Return[reconstruction];

	];




    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* L2Adjoint - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    L2Adjoint[ost3dObj_] := Module[

		{
	    	reconstruction 
    	},

	    (* Re-arange the orientation score to {theta,x,y,z } *)
      	reconstruction = Reorder[ost3dObj["FullData","Real"], "OrientationFirst"];
		
	    (* Correlate the os-layers with their corresponding wavelet *)
	    reconstruction = MapThread[LieAnalysis`OrientationScores3D`Construction`Private`WaveletTransform3D[#1,{Re[#2]}]&,{reconstruction, ost3dObj[["Wavelets"]]["FullData"]}];
		
	    (* Integrate over the orientation and multiply by the integration step-size *)
      	reconstruction = (Plus@@reconstruction) ( 4*Pi / ost3dObj[["Wavelets"]]["Orientations"]);
      	
      	(* Add the DC-component *)
      	reconstruction = reconstruction + ost3dObj[["DcData"]];
		
	    (* Return re-constructed volume *)
	    Return[reconstruction]

    ];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* Exact - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    Exact[ost3dObj_, threshold_:10^(-4)] := Module[

      	{
        	reconstruction, TMPsi, filtersF, Mpsi, MpsiInv 
      	},
    	
    	(* Re-arange the orientation score to {theta,x,y,z } *)
		reconstruction = Reorder[ost3dObj["FullData","Real"],"OrientationFirst"];
	
	    (* Correlate the os-layers with their corresponding wavelet *)
	    reconstruction = MapThread[LieAnalysis`OrientationScores3D`Construction`Private`WaveletTransform3D[#1,{Re[#2]}]&,{reconstruction, ost3dObj[["Wavelets"]]["FullData"]}];
	
      	(* Compute Mphi *)
      	filtersF = CenteredFourier/@ost3dObj[["Wavelets"]][["Data"]];
      	Mpsi = Total[Abs[filtersF]^2] + CenteredFourier[ost3dObj[["Wavelets"]][["DcFilter"]]]^2;

      	(* Chop to avoid division small numbers *)
      	TMPsi = Chop[Mpsi, threshold] /. {0 -> threshold};
      	MpsiInv = CenteredInverseFourier[TMPsi^(-1)];
		
		(* Integrate over the orientations and scale by integration step size *)
		reconstruction = (Plus@@reconstruction) ( 4*Pi / ost3dObj[["Wavelets"]]["Orientations"]);
		
      	(* Add the DC-component *)
      	reconstruction = reconstruction + ost3dObj[["DcData"]];
		
      	(* Add the dc-component and integrate the orientations *)
      	reconstruction = LieAnalysis`OrientationScores3D`Construction`Private`WaveletTransform3D[reconstruction,{MpsiInv}];

      	(* Undo construction and deconstruction effect of the used kernel *)
      	Return[reconstruction]

    ];



  End[];



EndPackage[];