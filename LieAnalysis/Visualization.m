(*************************************************************************************************************************
** Visualization.m
** This .m file contains common function used to store visualization function used in more than one specific area
**
** Author: M.H.J.Janssen <m.h.j.janssen@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
** Version: 0.1
**************************************************************************************************************************)
BeginPackage["LieAnalysis`Visualization`"];



  QuickVisualize::usage = "QuickVisualize[Object] visualises the object using its default visualization.";



  (*Todo: clean up Lie Style *)
  Downsampling::usage = "Downsampling is an option for several plotting functions that specifies the degree of dimensionality reduction employed in the visualization."
  GlyphFunction::usage = "GlyphFunction is an option for GlyphPlot that determines how the provided coefficients are mapped to glyphs.";
  GlyphScaling::usage = "GlyphScaling is an option for GlyphPlot that determines the scaling of the plotted glyphs.";
  OrientationConvention::usage = "OrientationConvention is an option for various plot function that specifies the used display conventionention.";
  SpatialCoordinateSystem::usage = "SpatialCoordinateSystem is an option for various plot function that specifies the coordinate system relative to which the input arguments should be interpreted.";
  SpatialTransformation::usage = "SpatialTransformation is an option for various plot function that specifies the TransformationFunction object that maps the virtual coordinates to real-world coordinates.";
  SpatialUnit::usage = "SpatialUnit is an option for various plot functions that specifies the units in which the real-world coordinates are expressed.";

  (*IntensityRange::usage = "IntensityRange is an option for various plot functions that specifies the numeric range of the data.";*)

  


  Begin["`Private`"];



    Needs["Classes`"];
    Get["LieAnalysis`Common`"];
    Needs["LieAnalysis`Visualization`CurveFunctions`"];
    Needs["LieAnalysis`Visualization`ColorFunctions`"];
    Needs["LieAnalysis`Visualization`GlyphFunctions`"];
    Needs["LieAnalysis`Visualization`Common`"];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* QuickVisualize- - - - - - - - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    QuickVisualize::msg = "Nothing to visualise here...";



    Options[QuickVisualize] = Options[Show];


	QuickVisualize[object_ObjScaleOrientationScores2D /; ValidQ[object, LieAnalysis`Objects`ObjScaleOrientationScores2D`ObjScaleOrientationScores2D], opts:OptionsPattern[]] := Module[
    		
    		{
    			orientationsPerScale = object["NumberOfLayers"]/object["Scales"],
    			osPerScale, scaleMIPs
    		},
    		
    		
    		(* Partition data per scale *)
    		osPerScale = Partition[object[["Data"]],Dimensions[object[["Data"]],2]~Append~orientationsPerScale][[1,1]];
    		
    		(* Create MIP's per scale *)
    		scaleMIPs = ImageAdjust[Image[LieAnalysis`Visualization`Private`MaximumIntensityProjection[#]]]&/@osPerScale;
    		
    		(* Combine to a single image *)
    		Return[Show[ColorCombine[scaleMIPs],opts]]
    		
    	]
    (* ObjWaveletTransform *)
    QuickVisualize[cwsObj_/; ValidQ[cwsObj, LieAnalysis`Objects`ObjWaveletTransform`ObjWaveletTransform], opts:OptionsPattern[]] := Module[

      (* Local Variables Declaration *)
      {
        data = cwsObj[["Data"]],
        image
      },

      (* Check if the data has been set *)
      If[!MatchQ[data,Automatic],
        image = ColorOrientationProjection[data, opts];
      ,
        image = Classes`Private`$ElisionsIcon;
      ];

      (* Return the image *)
      Return[image];
      

    ];



    (* Obj3DPositionOrientationData *)
    QuickVisualize[os3dObj_ /; ValidQ[os3dObj, LieAnalysis`Objects`Obj3DPositionOrientationData`Obj3DPositionOrientationData], opts: OptionsPattern[]] := Module[

      {},

      Show[
      	Graphics[Text["To be implemented"]], opts
      ]

    ];


    (* ObjOrientationWavelet *)
    QuickVisualize[cwsObj_ /; ValidQ[cwsObj, LieAnalysis`Objects`ObjOrientationWavelet`ObjOrientationWavelet], opts:OptionsPattern[]] :=
        Show[ImageAdjust[Image[Re@cwsObj[["Data"]][[1]]]], opts];


    (* ObjCakeWavelet3D *)
    QuickVisualize[Cake3DObj_ /; ValidQ[Cake3DObj, LieAnalysis`Objects`ObjCakeWavelet3D`ObjCakeWavelet3D], opts:OptionsPattern[]] := Module[

      {
        wRe = Re[Cake3DObj[["Data"]][[1]]],
        cs = ContourStyle -> {Directive[Blue, Opacity[0.8], Specularity[White, 30]], Directive[Orange, Opacity[0.8], Specularity[White, 30]]},
        otherOptions
      },

      otherOptions = Sequence @@ {ImageSize -> 150, Mesh -> False, cs, MaxPlotPoints -> 50};

      Show[
        ListContourPlot3D[
          wRe,
          otherOptions,
          Contours -> {0.155 Min[wRe], 0.06 Max[wRe]},
          PerformanceGoal->"Speed",
          Axes->False, Boxed->False,
          PlotRange ->Transpose[{{1, 1, 1}, Dimensions[wRe]}]
        ], opts
      ]
    ];


	(* TODO: 3D visualization *)
	QuickVisualize[tensorObj_ /; ValidQ[tensorObj, LieAnalysis`Objects`ObjTensor`ObjTensor], opts:OptionsPattern[]] := Module[
	
		{
			dim = Dimensions[tensorObj[["Data"]]],
			nSpatialDim = tensorObj[["SpatialDimensions"]],
			nOrientations, nTensors, primitive, coords, dPhi, elements, centerElement, counterElement
		},
		
		(* Compute properties *)
		nOrientations = dim[[nSpatialDim+1]];
		nTensors = Times@@dim[[nSpatialDim+2;;-1]];
		
		(* Determine which primitive could be used *)
		primitive = Switch[nTensors,
			1, Disk[{#1,#2},#3]&,
			2, {Arrowheads[.5#3],Thickness[.1#3],Arrow[{.3{#1,#2},{#1,#2}}]}&,
			3, Triangle[{{#1,#2+.3#3 },{#1-Sqrt[.3#3/2],#2-Sqrt[.3#3/2]},{#1+Sqrt[.3#3/2],#2-Sqrt[.3#3/2]}}]&,
			4, Rectangle[{#1,#2}-#3,{#1,#2}+#3]&,
			_, RegularPolygon[{#1,#2},.7#3,nTensors]&
		];
		
		(* Compute Coordinates on a circle *)
		dPhi = 2\[Pi]/nOrientations;
		coords = 0.7Table[{Cos[\[Theta]],Sin[\[Theta]]},{\[Theta],0,2\[Pi]-dPhi,dPhi}];
		
		(* Create Graphics objects *)
		elements = Graphics[{ColorData["Rainbow"][(Last@ToPolarCoordinates[#]+\[Pi])/(2\[Pi])],primitive[Sequence@@#,2/nOrientations]}&/@coords];
		
		(* The element shown in the center *)
		centerElement = Graphics[{Opacity[0.2],White,primitive[0,0,.6]}];
		
		(* Text element shows how many spatial elements there are *)
		counterElement = Graphics[Style[Text[ToString[Times@@(dim[[1;;nSpatialDim]])],{0,0}],"Subsubtitle",White,Larger, Bold]];
		
		(*ToDo: temp *)
		Show[
			Graphics[{Rectangle[{-1.1,-1.1},{1.1,1.1}]}],
			centerElement,
			elements,
			Graphics[Style[Text[ToString[Times@@(dim[[1;;nSpatialDim]])],{0,0}],"Subsubtitle",White,Larger, Bold]],
			opts
		]
	
	]	 



    QuickVisualize[obj_ /; ValidQ[obj, LieAnalysis`Objects`Object`Object], opts:OptionsPattern[]] :=
        QuickVisualize[obj[["Data"]], opts];



    QuickVisualize[data_List ? MatrixQ, opts:OptionsPattern[]] :=
        Show[ImageAdjust[Image[data]], opts];



    QuickVisualize[___, opts:OptionsPattern[]] :=
        Show[Classes`Private`$ElisionsIcon,opts];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* ColorOrientationProjection- - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    MaximumIntensityProjection[data_] := Map[Max[#]&,Abs[data],{2}];



    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
    (* ColorOrientationProjection- - - - - - - - - - - - - - - - *)
    (* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



    ColorOrientationProjection[data_,opts:OptionsPattern[]]:= Module[

      (* Local Variables Declaration *)
      {max, orientation},

      (* Compute Maximum Intensity Projection *)
      max = MaximumIntensityProjection[data];

      (* Compute the position of the maximum intensity at which orientation *)
      orientation = MapThread[First@First@Position[#1,#2]&,{Abs[data],max},2];

      (*Show the image *)
      Show[
        SetAlphaChannel[
          Colorize[ImageAdjust[Image[orientation]], ColorFunction -> (ColorData["Rainbow"][#]&)(*Function[orientation, Hue[orientation]]*)],
          Image[Rescale@max]
        ],opts
      ]

    ];



    



  End[];



EndPackage[];