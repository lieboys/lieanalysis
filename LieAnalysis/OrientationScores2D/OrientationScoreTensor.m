(*************************************************************************************************************************
** OrientationScoreTensor.m
** This .m file contains functions used to compute 'tensors' from the SE(3) space.
**
** Author: F.C. Martin <f.c.martin@tue.nl>
**************************************************************************************************************************)
BeginPackage["LieAnalysis`OrientationScores2D`OrientationScoreTensor`",{
  "LieAnalysis`"
}];



  	Begin["`Private`"];



	    Needs["Classes`"];
	    Get["LieAnalysis`Common`"];

		
		
		Options[OrientationScoreTensor] = Options[LeftInvariantDerivatives];



		OrientationScoreTensor[osObj_ /; ValidQ[osObj, LieAnalysis`Objects`ObjPositionOrientationData`ObjPositionOrientationData], {sigmaSpatial_, sigmaOrientation_}, derivativeIndex_ /; MatchQ[derivativeIndex, {_Integer...}], opts:OptionsPattern[] ] := Block[
			
			{
				derivatives, tensor, labels
			},
			
			(* Compute the derivates *)
			derivatives = Transpose[#["Data"],{2,3,1}]&/@LeftInvariantDerivatives[osObj, {sigmaSpatial, sigmaOrientation}, derivativeIndex, opts];
			
			(* Re-order to tensor format *)
			tensor = Transpose[derivatives, {4, 3, 1, 2}];
			
			(* Set the labels *)			
			labels = Table["A" <> (ToString[#] & /@ Riffle[IntegerDigits[dir], "A"]),{dir,derivativeIndex}];
			
			(* Put into ObjTensor *)
			Return[
				LieAnalysis`Objects`ObjTensor`ObjTensor[{
					"Data"->tensor,
					"OrientationScore"->osObj,
					"Labels"->labels,
					"SpatialDimensions"->2,
					"Metadata"-><|"SigmaSpatial"->sigmaSpatial, "SigmaOrientation"->sigmaOrientation|>,
					"SpecifiedOptions"->{opts}
				}]
			];
			
		]
	
	
	
		OrientationScoreTensor[osObj_ /; ValidQ[osObj, LieAnalysis`Objects`ObjPositionOrientationData`ObjPositionOrientationData], {sigmaSpatial_, sigmaOrientation_}, derivativeIndex_String, opts:OptionsPattern[] ] := Block[
			
			{
				tensorObj		
			},
			
			(* Compute the Tensor *)
			Switch[ToUpperCase@derivativeIndex,
				
				"GRADIENT", (
				
			 		tensorObj = OrientationScoreTensor[osObj, {sigmaSpatial, sigmaOrientation}, {1,2,3}, opts];

				),
				
				"HESSIAN", (
					
					tensorObj = OrientationScoreTensor[osObj, {sigmaSpatial, sigmaOrientation}, {11,21,31,12,22,32,13,23,33}, opts];
					
					(* Reshape to from vector to matrix *)
					AffixTo[tensorObj, "Data"->ArrayReshape[tensorObj[["Data"]],Dimensions[tensorObj[["Data"]],3]~Join~{3,3}]];
					AffixTo[tensorObj, "Labels"->ArrayReshape[tensorObj[["Labels"]],{3,3}]];
					
				),
				
				"STRUCTURETENSOR", (
					
					tensorObj = OrientationScoreTensor[osObj, {sigmaSpatial, sigmaOrientation}, "GRADIENT", opts];
					
					(* Compute structure tensor *)
					Block[
						
						{
							A1 = tensorObj[["Data"]][[All,All,All,1]],
							A2 = tensorObj[["Data"]][[All,All,All,2]],
							A3 = tensorObj[["Data"]][[All,All,All,3]],
							A1A2,A2A3,A1A3
						},
					
						AffixTo[tensorObj,"Data"->Transpose[{{A1^2, A1A2 = A1 A2, A1A3 = A1 A3}, {A1A2, A2^2, A2A3 = A2 A3}, {A1A3, A2A3, A3^2}},{4,5,1,2,3}]];
						AffixTo[tensorObj, "Labels"->{{"A1^2", "A1*A2", "A1*A3"},{"A1*A2","A2^2","A2*A3"},{"A1*A3","A2*A3","A3^2"}}];
						
					];
					
					
				)
				
			];
			
			(* Return the tensor *)
			Return[tensorObj];
			
		]
	   
	    
	End[];



EndPackage[];