(*************************************************************************************************************************
** GaborWavelets.m
** This .m file contains functions to create gabor-wavelets
** 
** Author: E.J. Bekkers <e.j.bekkers@tue.nl>
** Author: F.C. Martin <f.c.martin@tue.nl>
** Version: 0.1	
**************************************************************************************************************************)
BeginPackage["LieAnalysis`OrientationScores2D`Filters`GaborWavelets`"];



	Begin["`Private`"];



		Needs["Classes`"];
		Needs["LieAnalysis`Objects`ObjGaborWavelet`"];
		Get["LieAnalysis`Common`"];
		
		
		
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* GaborWaveletStack - - - - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



		Options[GaborWaveletStack] = {
			"Epsilon" -> 4, (* Defines the anisotropy of the wavelet *)
			"K0" -> {0, 3},
			"Directional" -> False
		};



		GaborWaveletStack::invalidInput = "The supplied value '`1`' for option `2` is invalid. (`3`)";



		GaborWaveletStack[nOrientations_, scale_, sizeIn_:Automatic, opts:OptionsPattern[]] := Module[
			
			(* Local Variables Declaration *)	
			{
				k0 								= OptionValue["K0"],
				epsilon 					= OptionValue["Epsilon"],
				directional 			= OptionValue["Directional"],
				size, gaborObj
			},

			(* Compute Automatic size *)
			size = If[sizeIn === Automatic,
				Floor[8*Sqrt[epsilon]*scale],
				sizeIn
			];
			size = If[EvenQ[size], size + 1, size];

			(* Validate the Input *)
			And[

				(OddQ[size] && Internal`PositiveIntegerQ[size] )
					/. False :> (Message[GaborWaveletStack::invalidInput, size, "size", "Sould be an odd positive integer"]; False),

				Internal`PositiveIntegerQ[nOrientations]
					/. False :> (Message[CakeWaveletStack::invalidInput, nOrientations, "nOrientations", "Should be a positive integer"]; False),

				(NumericQ[scale] && Positive[scale])
					/. False :> (Message[GaborWaveletStack::invalidInput, scale, "scale", "Should be a positive number"]; False),

				MatchQ[k0, {_,_}](*ToDo: VectorQ[,2]*)
					/. False :> (Message[GaborWaveletStack::invalidInput,k0,"K0","Should be a vector2"]; False),

				Positive[epsilon]
					/. False :> (Message[GaborWaveletStack::invalidInput, epsilon, "Epsilon","Should be a positive number"]; False),

				BooleanQ[directional]
					/. False :> (Message[GaborWaveletStack::invalidInput, directional, "Directional", "Should be a Boolean"]; False)

			] /. False :> Abort[];

			(* Compute Gabor-Wavelet Stack *)
			gaborObj = GaborWaveletStack[size, nOrientations, scale, k0, epsilon, directional];
			
			(* Append the used options to the object *)
			AffixTo[gaborObj,"SpecifiedOptions"->{opts}];
			
		 	(* Return the stack *)
			Return[gaborObj]

		];



		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* GaborWaveletStack - - - - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



		GaborWaveletStack[size_, nOrientations_, scale_, k0_, epsilon_, directional_] := Module[

			(* Local Variable Declaration *)
			{
				noSymmetry = OddQ[nOrientations],
				gaborObj, wavelets, A
			},

			(* Creating container to save wavelet *)
			gaborObj = ObjGaborWavelet[];

			(* Save Options *)
			gaborObj = Affix[gaborObj, {
				"WaveletSize"->size,
				"Scale"->scale,
				"K0"->k0,
				"Epsilon"->epsilon,
				"Directional"->directional
			}];

			(* Compute A matrix *)
			A =({{epsilon^(-1/2), 0}, {0, 1}});

			(* Compute the Gabor Wavelet Stack *)
  			wavelets = GenerateKernelSetGabor[A, k0, scale, N[2Pi/nOrientations], size, noSymmetry];
  			gaborObj = Affix[gaborObj, "PreSpatialFilters"->wavelets];

  			(* Cut th single wavelets in two directional wavelets *)
  			If[directional,
  				wavelets = If[noSymmetry, wavelets, Join[wavelets, Conjugate[wavelets]]];
    			wavelets = wavelets * LieAnalysis`OrientationScores2D`Filters`CakeWavelets`Private`ErfSet[size, nOrientations, 2Pi];
  			];

  			AffixTo[gaborObj, {
  				"Data"->Developer`ToPackedArray@wavelets,
  				"Symmetry"->If[noSymmetry || directional, 2Pi, Pi]
  			}];

			(* Return Gabor-wavelets *)
			Return[gaborObj]


		];


		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)
		(* GenerateKernelSetGabor- - - - - - - - - - - - - - - - - - *)
		(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)



		GenerateKernelSetGabor = Compile[
			
			{
				{A, _Real, 2}, {k0, _Real, 1}, {a, _Real}, {dTheta, _Real}, {dim, _Real}, {noSymmetry, True|False}
			},
  		
  			Module[
  				
  				{
  					s = If[noSymmetry, 0, Pi]
  				},

				(* Construct the wavelets *)
   				$MachineEpsilon + (1/((4 a Pi)/E^(9/2))) (1/a)*Table[
	     	 		Table[
	     	 			With[
		     	 				{
		         				xx = {Cos[-Theta + Pi/2],-Sin[-Theta + Pi/2]}.{x, y},
		         				yy = {Sin[-Theta + Pi/2], Cos[-Theta + Pi/2]}.{x, y}
		         			},
		        			Exp[I (k0.({yy, xx}/a))] Exp[-(1/2) Norm[(A.({yy, xx}/a))]^2]
						],
	       				{x, -Floor[dim/2], Floor[dim/2] - (1 - 2*Mod[dim/2, 1])},
	       				{y, -Floor[dim/2], Floor[dim/2] - (1 - 2*Mod[dim/2, 1])}
	       			],
      			{Theta, s, 2*Pi-dTheta, dTheta}
				]
   			]
   			
  		];
  	
  	
	  
	End[];
  
  

EndPackage[];