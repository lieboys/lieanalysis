(* Paclet Info File *)

(* created 2017/10/26*)

Paclet[
    Name -> "LieAnalysis",
    Version -> "1.0.0",
    MathematicaVersion -> "10+",
    Description -> "Release 1",
    Extensions -> 
        {
            {"Documentation", Language -> "English", LinkBase -> "LieAnalysis", MainPage -> "Guides/LieAnalysis"}, 
            {"Kernel"}
        }
]


